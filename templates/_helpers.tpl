{{/*
Expand the name of the chart.
*/}}
{{- define "AppCtx.name" }}
{{- $env_name := "" }}
{{- if .Values.env_name }}
    {{- $env_name = printf "-%s" .Values.env_name }}
{{- end }}
{{- default "cdb" .Chart.Name | trunc 63 | trimSuffix "-" }}
{{- end }}


{{- define "AppCtx.name.db" }}
{{- $env_name := "" }}
{{- if .Values.env_name }}
    {{- $env_name = printf "-%s" .Values.env_name }}
{{- end }}
{{- include "AppCtx.name" . }}{{ $env_name }}-db
{{- end }}

{{- define "AppCtx.team.db" }}
{{- $env_name := "" }}
{{- if .Values.env_name }}
    {{- $env_name = printf "-%s" .Values.env_name }}
{{- end }}
{{- include "AppCtx.name" . }}{{ $env_name }}
{{- end }}

{{- define "AppCtx.name.api" }}
{{- $env_name := "" }}
{{- if .Values.env_name }}
    {{- $env_name = printf "-%s" .Values.env_name }}
{{- end }}
{{- include "AppCtx.name" . }}{{ $env_name }}-api
{{- end }}


{{- define "AppCtx.name.front" }}
{{- $env_name := "" }}
{{- if .Values.env_name }}
    {{- $env_name = printf "-%s" .Values.env_name }}
{{- end }}
{{- include "AppCtx.name" . }}{{ $env_name }}-front
{{- end }}


{{- define "AppCtx.fqdn.api" }}
{{- $env_name := "" }}
{{- if .Values.env_name }}
    {{- $env_name = printf ".%s" .Values.env_name }}
{{- end }}
{{- printf "api%s.%s" $env_name .Values.domain_name }}
{{- end }}


{{- define "AppCtx.fqdn.front" }}
{{- $env_name := "" }}
{{- if .Values.env_name }}
    {{- $env_name = printf ".%s" .Values.env_name }}
{{- end }}
{{- printf "www%s.%s" $env_name .Values.domain_name }}
{{- end }}


{{- define "AppCtx.url.api" }}
  {{- $apiPattern := "" }}
  {{- if .Values.api.ingress.tlsEnabled }}
    {{- $apiPattern = "https://%s" }}
  {{- else }}
    {{- $apiPattern = "http://%s" }}
  {{- end }}
  {{- printf $apiPattern (include "AppCtx.fqdn.api" .) }}
{{- end }}